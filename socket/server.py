#Biblioteca para códigos simultâneos
import asyncio
#Biblioteca para criação do server
import websockets
#Importando função criada para gerar números aleatórios
from random_num import generator
#Biblioteca para gerenciar o bando de dados
from sqlalchemy import *
#Direcionados para logs.txt
import logging

#Configurando o logging
logging.basicConfig(filename ='logs.txt',
                    level = logging.DEBUG)

#Mesmo processo de acesso e criação de tabelas explicado no api/db.py
engine = create_engine('postgresql://anuar:anuar@db:5432/dbsocket')
metadata = MetaData()

table = Table('Random_Num', metadata,
            Column('id', Integer, primary_key=True, autoincrement=True),
            Column('Number', String))
metadata.create_all(engine)


async def time(websocket, path):
    while True:
        #Chamando o gerador de numero randômico
        new_data = generator()
        #Inserindo dados no banco a cada segundo
        ins = table.insert().values(Number= new_data)
        conn = engine.connect()
        conn.execute(ins)
        #Enviando dados para jscript
        await websocket.send(new_data)
        #Espera de um minuto para nova execução
        await asyncio.sleep(1)
        #Confirmando execução do processo e armazenando em logs.txt
        logging.debug('Entered values')
        logging.debug('Saved values')

#Porta e host do websocket para posterior acesso
start_server = websockets.serve(time, "0.0.0.0", 5600)

#Sistema de loop para atualização dos valores no html e inserção no banco de dados
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()