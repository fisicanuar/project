#Biblioteca responsável pela conexão, criação de tabelas e inserção de dados no banco. 
from sqlalchemy import *

#Engine se conectará com a database criado por default. Senha: anuar
engine = create_engine('postgresql://anuar:anuar@db:5432/dbflask')

#Estrutura do banco
metadata = MetaData()

#Modelo de tabela. Uma coluna chamada Date onde serão inseridos os dados a cada chamada do Webservice
table = Table('Date_and_Hour', metadata,
            Column('Date', String, primary_key=True))