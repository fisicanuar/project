#Biblioteca para acessar o valor do json
import requests
#Biblioteca para trabalhar o json recebido
import json


def get_date():
    #Requisitando o webservice
    r = requests.get('http://localhost:5000/date/json')
    #Lendo o json
    d = json.loads(r.text)
    #Retornando a data e hora
    return d['date']