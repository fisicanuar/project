#Flask para renderizar o html criado
from flask import Flask, render_template
#Restful para trafegar os dados no formato json
from flask_restful import Resource, Api
#Datetime que será usada para a chamada da hora atual
from datetime import datetime
#Chamando função que retorna a hora atual a partir da chamada do webservice
from request import get_date
#Logs serão salvos no logs.txt da pasta correspondente a este módulo
import logging

#Configurando logs para logs.txt e nivel de informações
logging.basicConfig(filename ='logs.txt',
                    level=logging.DEBUG)

#Criação app e api
app = Flask(__name__)
api = Api(app)

#Classe que herda de Resource e que apenas utiliza-se do método get para disparar as ações
class Date(Resource):
    def get(self):
        d=datetime.now()
        #INSERT INTO (tabela criada no módulo db.py) VALUES (datetime.now())
        ins = table.insert().values(Date= d)
        #Conecta-se a engine para execução do insert
        conn = engine.connect()
        #Execução do insert
        conn.execute(ins)
        #Log adicional para confirmação dos dados salvos
        logging.info('Saved dates')
        #Retornando um dict que será transformado em json para exibição
        return dict(date= str(d))
#Adicionando a rota para disparar os eventos acima
api.add_resource(Date, '/date/json')
    
#Simples rota para consultar o Webservice
@app.route('/date')
def get_html_date():
    #Log adicional para confirmar execução
    logging.info('Show html')
    #Retorna um html que carrega a variável "d", que depende do Webservice
    return render_template('date.html', d= get_date())

#Criando engine apenas a partir do módulo db.py .Tudo é criado apenas na inicialização do api.py
if __name__=='__main__':
    from db.db import engine, table, metadata
    metadata.create_all(engine)
    #Rodando na porta e host padrão.  
    app.run(debug=True, port= 5000, host='0.0.0.0')