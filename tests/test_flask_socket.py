import requests

def test_date_return_status_code_200_when_api_is_running():
    r = requests.get('http://localhost:5000/date')
    assert r.status_code==200

def test_date_json_return_status_code_200_when_api_is_running():
    r = requests.get('http://localhost:5000/date/json')
    assert r.status_code==200

def test_port_5000_return_status_code_404_when_not_date_json_and_api_is_running():
    r = requests.get('http://localhost:5000/something')
    assert r.status_code==404

def test_port_5600_return_code_426_cause_server_is_running():
    r = requests.get('http://localhost:5600')
    assert r.status_code==426